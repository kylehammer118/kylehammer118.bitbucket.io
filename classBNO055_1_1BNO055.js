var classBNO055_1_1BNO055 =
[
    [ "__init__", "classBNO055_1_1BNO055.html#a5183ceea62c779964d031151a5a96cc4", null ],
    [ "calibration_status", "classBNO055_1_1BNO055.html#a64feeef47b9517312a4bf1092726efec", null ],
    [ "read_angular_vel", "classBNO055_1_1BNO055.html#a69d3a23b3b89f0c45fac26dcf8411f87", null ],
    [ "read_euler", "classBNO055_1_1BNO055.html#ac5b1e099cdb65087ffa00d098fbe6c4c", null ],
    [ "retrieve_calibration_coef", "classBNO055_1_1BNO055.html#a5a3b9bdd84e7f23a2dd333d1c5f4ad03", null ],
    [ "set_operating_mode", "classBNO055_1_1BNO055.html#a4b0915b699b39085be47ba99626db225", null ],
    [ "write_calibration_coef", "classBNO055_1_1BNO055.html#ad20ea2ad9b34ba6b1b00a64e1e2eaa25", null ],
    [ "eul_bytes", "classBNO055_1_1BNO055.html#ac1dcb4c4bdefd3e767294f93a733967c", null ],
    [ "I2C_object", "classBNO055_1_1BNO055.html#a7b8bdfb2a0cddff73f60cf1e71f94826", null ],
    [ "pitch", "classBNO055_1_1BNO055.html#aece175efeac88d3ea804af526ca455f9", null ],
    [ "roll", "classBNO055_1_1BNO055.html#a034f934bf79f3499d4f174f6c4235345", null ],
    [ "vel_bytes", "classBNO055_1_1BNO055.html#acdfe52c447e9dc43681c2d9251376162", null ],
    [ "vel_x", "classBNO055_1_1BNO055.html#afb35e5341d108d003097cf21cabc002a", null ],
    [ "vel_y", "classBNO055_1_1BNO055.html#a3daac019d591daedc2c85733fc9ffc5a", null ],
    [ "vel_z", "classBNO055_1_1BNO055.html#a0572340f53d6d9596cdea530b901c2b8", null ],
    [ "yaw", "classBNO055_1_1BNO055.html#a7126a065ed256a9694f655b6bd166df0", null ]
];