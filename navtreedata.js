/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "My Project", "index.html", [
    [ "ME305 Labs", "index.html", [
      [ "Introduction", "index.html#sec_Intro", null ],
      [ "Lab 0x01", "index.html#sec_Lab1", null ],
      [ "Lab 0x02", "index.html#sec_Lab2", null ],
      [ "Lab 0x03", "index.html#sec_Lab3", null ],
      [ "Lab 0x04", "index.html#sec_Lab4", null ],
      [ "Lab 0x05", "index.html#sec_4", null ]
    ] ],
    [ "ME305 Homework", "1.html", [
      [ "HW0x02", "1.html#sec_1", null ],
      [ "HW0x03", "1.html#sec_2", null ]
    ] ],
    [ "ME305 Term Project", "2.html", [
      [ "ME 305 Term Project", "2.html#sec_i", null ],
      [ "Task Diagram", "2.html#sec_task", null ],
      [ "Task User", "2.html#sec_user", null ],
      [ "Task Touch", "2.html#sec_touch", null ],
      [ "Task IMU", "2.html#sec_IMU", null ],
      [ "Task Closed Loop", "2.html#sec_CL", null ],
      [ "Task Motor", "2.html#sec_motor", null ],
      [ "Demonstrational Video", "2.html#vid", null ],
      [ "X and Y Plots from Task Touch", "2.html#plots", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"1.html",
"classtask__user1_1_1Task__User.html#aa2226aaf4084668209623eca1f4049ee"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';