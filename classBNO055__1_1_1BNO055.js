var classBNO055__1_1_1BNO055 =
[
    [ "__init__", "classBNO055__1_1_1BNO055.html#abda16c750252363b978512d1b953f888", null ],
    [ "calibration_status", "classBNO055__1_1_1BNO055.html#ac6e286ae36855e471da0355a59bff9ed", null ],
    [ "read_angular_vel", "classBNO055__1_1_1BNO055.html#a691a1cc9aee544178a789439c3034c27", null ],
    [ "read_euler", "classBNO055__1_1_1BNO055.html#a9840b695dec7105613aaf17de4e9bf79", null ],
    [ "retrieve_calibration_coef", "classBNO055__1_1_1BNO055.html#a6a5eeba67f9ba1ff02103b89372fbe22", null ],
    [ "set_operating_mode", "classBNO055__1_1_1BNO055.html#a2f1c4f11bcd96c991633c8f246fcc6f9", null ],
    [ "write_calibration_coef", "classBNO055__1_1_1BNO055.html#af6bd5edc31862a51511ec9201c838944", null ],
    [ "eul_bytes", "classBNO055__1_1_1BNO055.html#a9c452cae8a1c575975b6d4cca8491723", null ],
    [ "I2C_object", "classBNO055__1_1_1BNO055.html#af6eda0b738630fc5ba9cd3c07ca528c6", null ],
    [ "pitch", "classBNO055__1_1_1BNO055.html#ad88b70938a57ac331ecfb57871b1bd4a", null ],
    [ "roll", "classBNO055__1_1_1BNO055.html#ad04ba4e57c5eda8d83bd98dae70d9a16", null ],
    [ "vel_bytes", "classBNO055__1_1_1BNO055.html#adc4b51eea7e2f4dc5da3237b02ef7d34", null ],
    [ "vel_x", "classBNO055__1_1_1BNO055.html#a42cbc34a1e299e13004a62e7f57d1d82", null ],
    [ "vel_y", "classBNO055__1_1_1BNO055.html#a1bc1de948068bbe906797adb4edd2ce7", null ],
    [ "vel_z", "classBNO055__1_1_1BNO055.html#ae211facfc84a15e3bb661e3be70766fd", null ],
    [ "yaw", "classBNO055__1_1_1BNO055.html#ae54dac56f09e320a0c430784b9ff1918", null ]
];