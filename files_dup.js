var files_dup =
[
    [ "BNO055.py", "BNO055_8py.html", [
      [ "BNO055.BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "BNO055_1.py", "BNO055__1_8py.html", [
      [ "BNO055_1.BNO055", "classBNO055__1_1_1BNO055.html", "classBNO055__1_1_1BNO055" ]
    ] ],
    [ "closedloop.py", "closedloop_8py.html", [
      [ "closedloop.ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "closedloop1.py", "closedloop1_8py.html", [
      [ "closedloop1.ClosedLoop", "classclosedloop1_1_1ClosedLoop.html", "classclosedloop1_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847.py", "DRV8847_8py.html", "DRV8847_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "Lab0x02.py", "Lab0x02_8py.html", "Lab0x02_8py" ],
    [ "main1.py", "main1_8py.html", "main1_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "motor_driver1.py", "motor__driver1_8py.html", [
      [ "motor_driver1.Motor", "classmotor__driver1_1_1Motor.html", "classmotor__driver1_1_1Motor" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_closedloop.py", "task__closedloop_8py.html", [
      [ "task_closedloop.Task_ClosedLoop", "classtask__closedloop_1_1Task__ClosedLoop.html", "classtask__closedloop_1_1Task__ClosedLoop" ]
    ] ],
    [ "task_closedloop1.py", "task__closedloop1_8py.html", [
      [ "task_closedloop1.Task_ClosedLoop", "classtask__closedloop1_1_1Task__ClosedLoop.html", "classtask__closedloop1_1_1Task__ClosedLoop" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", [
      [ "task_encoder.Task_Enc", "classtask__encoder_1_1Task__Enc.html", "classtask__encoder_1_1Task__Enc" ]
    ] ],
    [ "task_imu1.py", "task__imu1_8py.html", [
      [ "task_imu1.Task_IMU", "classtask__imu1_1_1Task__IMU.html", "classtask__imu1_1_1Task__IMU" ]
    ] ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_motor1.py", "task__motor1_8py.html", [
      [ "task_motor1.Task_Motor", "classtask__motor1_1_1Task__Motor.html", "classtask__motor1_1_1Task__Motor" ]
    ] ],
    [ "task_touch1.py", "task__touch1_8py.html", [
      [ "task_touch1.Task_Touch", "classtask__touch1_1_1Task__Touch.html", "classtask__touch1_1_1Task__Touch" ]
    ] ],
    [ "task_user.py", "task__user_8py.html", [
      [ "task_user.Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ],
    [ "task_user1.py", "task__user1_8py.html", [
      [ "task_user1.Task_User", "classtask__user1_1_1Task__User.html", "classtask__user1_1_1Task__User" ]
    ] ],
    [ "touch1.py", "touch1_8py.html", [
      [ "touch1.Panel", "classtouch1_1_1Panel.html", "classtouch1_1_1Panel" ]
    ] ]
];