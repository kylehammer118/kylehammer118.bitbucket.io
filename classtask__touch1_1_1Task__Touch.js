var classtask__touch1_1_1Task__Touch =
[
    [ "__init__", "classtask__touch1_1_1Task__Touch.html#a026a805deb51abd9f00833bbf3bafaf9", null ],
    [ "run", "classtask__touch1_1_1Task__Touch.html#a3377baee49ff71333d008057061ac538", null ],
    [ "transition_to", "classtask__touch1_1_1Task__Touch.html#ae55a039148aac7641a6a9003deb8ba13", null ],
    [ "currentx", "classtask__touch1_1_1Task__Touch.html#ad1d60291e2c070a9e04e511ba9dcd155", null ],
    [ "currenty", "classtask__touch1_1_1Task__Touch.html#a3b9b3b17957eb9f71dcff64df26dc310", null ],
    [ "dbg", "classtask__touch1_1_1Task__Touch.html#a2c6698231f091d5978504993a0c87f78", null ],
    [ "hold_time", "classtask__touch1_1_1Task__Touch.html#afe22a4ae014790aab75f28e69df6fd8e", null ],
    [ "hold_x", "classtask__touch1_1_1Task__Touch.html#aa371273d5ccb7338bece4e373a744b92", null ],
    [ "hold_y", "classtask__touch1_1_1Task__Touch.html#a736f368c30e0cafc96134aac59630885", null ],
    [ "name", "classtask__touch1_1_1Task__Touch.html#ab470fa009437053894bf53727930e268", null ],
    [ "next_time", "classtask__touch1_1_1Task__Touch.html#a8ed677440a2caea5b6e5ac6338396e71", null ],
    [ "period", "classtask__touch1_1_1Task__Touch.html#a9d138d7958af089e5e0efdbb6dd3f906", null ],
    [ "runs", "classtask__touch1_1_1Task__Touch.html#af5c1682a0f3841548de50c430eb633c3", null ],
    [ "state", "classtask__touch1_1_1Task__Touch.html#a0cbebfa8a9416055033ba44f26c6423e", null ],
    [ "time", "classtask__touch1_1_1Task__Touch.html#a6fb70e6b2d4d788b5b5bf06d583301e6", null ],
    [ "touch_object", "classtask__touch1_1_1Task__Touch.html#a55b3350ec00ee514a7858f89487f3d85", null ],
    [ "x", "classtask__touch1_1_1Task__Touch.html#a6f3b64077668062eef32bc7a24d7f8d7", null ],
    [ "xd", "classtask__touch1_1_1Task__Touch.html#ad782ad824fb6a4e66bc5e66e5690ded4", null ],
    [ "y", "classtask__touch1_1_1Task__Touch.html#a70aa84a056f2b1c68761c2e152679134", null ],
    [ "yd", "classtask__touch1_1_1Task__Touch.html#a0100d622b0f8038e5d546bbbce5d8bf2", null ],
    [ "z", "classtask__touch1_1_1Task__Touch.html#a9d50db3d7f1d185493eea849413578cb", null ]
];