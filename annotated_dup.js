var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "BNO055_1", null, [
      [ "BNO055", "classBNO055__1_1_1BNO055.html", "classBNO055__1_1_1BNO055" ]
    ] ],
    [ "closedloop", null, [
      [ "ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "closedloop1", null, [
      [ "ClosedLoop", "classclosedloop1_1_1ClosedLoop.html", "classclosedloop1_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "motor_driver1", null, [
      [ "Motor", "classmotor__driver1_1_1Motor.html", "classmotor__driver1_1_1Motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_closedloop", null, [
      [ "Task_ClosedLoop", "classtask__closedloop_1_1Task__ClosedLoop.html", "classtask__closedloop_1_1Task__ClosedLoop" ]
    ] ],
    [ "task_closedloop1", null, [
      [ "Task_ClosedLoop", "classtask__closedloop1_1_1Task__ClosedLoop.html", "classtask__closedloop1_1_1Task__ClosedLoop" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Enc", "classtask__encoder_1_1Task__Enc.html", "classtask__encoder_1_1Task__Enc" ]
    ] ],
    [ "task_imu1", null, [
      [ "Task_IMU", "classtask__imu1_1_1Task__IMU.html", "classtask__imu1_1_1Task__IMU" ]
    ] ],
    [ "task_motor", null, [
      [ "Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_motor1", null, [
      [ "Task_Motor", "classtask__motor1_1_1Task__Motor.html", "classtask__motor1_1_1Task__Motor" ]
    ] ],
    [ "task_touch1", null, [
      [ "Task_Touch", "classtask__touch1_1_1Task__Touch.html", "classtask__touch1_1_1Task__Touch" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ],
    [ "task_user1", null, [
      [ "Task_User", "classtask__user1_1_1Task__User.html", "classtask__user1_1_1Task__User" ]
    ] ],
    [ "touch1", null, [
      [ "Panel", "classtouch1_1_1Panel.html", "classtouch1_1_1Panel" ]
    ] ]
];